﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using worksservice.Data;
using Microsoft.EntityFrameworkCore;
using worksservice.Repositories;
using worksservice.Services;
using Minio;
using worksservice.Constants;
using worksservice.Helpers;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace worksservice
{
    public class Startup
    {
        private readonly string contentRoot;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            contentRoot = env.ContentRootPath;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Work service",
                    Description = "Service for managing students ' work (practice, laboratory work, other work)",
                    Contact = new OpenApiContact
                    {
                        Name = "Solovev Mihail",
                        Email = "youoliver@mail.ru",
                        Url = new Uri("https://vk.com/guyfromparadise")
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(contentRoot, xmlFile);
                c.IncludeXmlComments(xmlPath);

                // Set auth params
                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer",
                    In = ParameterLocation.Header
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            });

            services.AddDbContext<DataContext>(options => options.UseNpgsql(Configuration.GetConnectionString("WorkDbConnection")));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);

            services.AddTransient<IFileRepository, FileRepository>();
            services.AddTransient<IWorkRepository, WorkRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();

            services.AddTransient<IFileService, FileService>();
            services.AddTransient<FileUploadSettings>();
            services.AddTransient(m => new MinioClient(
                Configuration["MinIO:Endpoint"],
                Configuration["MinIO:AccessKey"],
                Configuration["MinIO:SecretKey"])
                    .WithSSL());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Works service v1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

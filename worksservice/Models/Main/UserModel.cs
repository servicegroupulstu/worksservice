﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace worksservice.Models.Main
{
    /// <summary>
    /// Модель пользователя.
    /// </summary>
    [Table("users", Schema = "main")]
    public class UserModel
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        [Required]
        [Column("login")]
        public string Login { get; set; }

        /// <summary>
        /// Электронная почта.
        /// </summary>
        [Required]
        [Column("email")]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        [Required]
        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [Required]
        [Column("password")]
        public string Password { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [Required]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [Required]
        [Column("surname")]
        public string Surname { get; set; }

        /// <summary>
        /// Аватар.
        /// </summary>
        [Column("avatar")]
        public string Avatar { get; set; }

        /// <summary>
        /// ИД роли.
        /// </summary>
        [Required]
        [Column("role_id")]
        public int RoleId { get; set; }

        /// <summary>
        /// ИД группы.
        /// </summary>
        [Required]
        [Column("group_id")]
        public int GroupId { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        [Column("created_at")]
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Column("updated_at")]
        public DateTime? Updated { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace worksservice.Models.Main
{
    /// <summary>
    /// Модель предмета.
    /// </summary>
    [Table("subjects", Schema = "main")]
    public class SubjectModel
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Название предмета.
        /// </summary>
        [Required]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание предмета.
        /// </summary>
        [Column("description")]
        public string Description { get; set; }

        /// <summary>
        /// ИД преподователя, который ведет данный предмет.
        /// </summary>
        [Required]
        [Column("user_id")]
        public int UserId { get; set; }

        /// <summary>
        /// Преподаватель, который ведет данный предмет.
        /// </summary>
        [Required]
        [ForeignKey("user_id")]
        public UserModel Teacher { get; set; }

        /// <summary>
        /// Тип экзамена.
        /// </summary>
        [Column("exam_type")]
        public string ExamType { get; set; }

        /// <summary>
        /// Условия экзамена.
        /// </summary>
        [Column("exam_condition")]
        public string ExamCondition { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        [Column("created_at")]
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Column("updated_at")]
        public DateTime? Updated { get; set; }
    }
}

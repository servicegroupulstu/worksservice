﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace worksservice.Models.Work
{
    /// <summary>
    /// Связь задания и файла.
    /// </summary>
    [Table("task_files", Schema = "work")]
    public class TaskFileModel
    {
        /// <summary>
        /// ИД задания.
        /// </summary>
        [Column("task_id")]
        public int TaskId { get; set; }

        /// <summary>
        /// Задание.
        /// </summary>
        [Required]
        [ForeignKey("task_id")]
        public TaskModel Task { get; set; }

        /// <summary>
        /// ИД файла.
        /// </summary>
        [Required]
        [Column("file_id")]
        public int FileId { get; set; }

        /// <summary>
        /// Файл.
        /// </summary>
        [Required]
        [ForeignKey("file_id")]
        public FileModel File { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace worksservice.Models.Work
{
    /// <summary>
    /// Модель файла.
    /// </summary>
    [Table("files", Schema = "work")]
    public class FileModel
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Название файла в хранилище.
        /// </summary>
        [Required]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// Отображаемое название файла.
        /// </summary>
        [Required]
        [Column("display_name")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Тип контента.
        /// </summary>
        [Required]
        [Column("content_type")]
        public string ContentType { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        [Column("created_at")]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Required]
        [Column("updated_at")]
        public DateTimeOffset Updated { get; set; }
    }
}

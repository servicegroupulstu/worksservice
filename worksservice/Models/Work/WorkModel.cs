﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace worksservice.Models.Work
{
    /// <summary>
    /// Модель работы студента.
    /// </summary>
    [Table("works", Schema = "work")]
    public class WorkModel
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        [Column("description")]
        public string Description { get; set; }

        /// <summary>
        /// Выполнена.
        /// </summary>
        [Column("is_done")]
        public bool? IsDone { get; set; }

        /// <summary>
        /// Оценка.
        /// </summary>
        [Column("mark")]
        public int? Mark { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        [Column("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// ИД файла.
        /// </summary>
        [Required]
        [Column("file_id")]
        public int FileId { get; set; }

        /// <summary>
        /// Файл.
        /// </summary>
        [Required]
        [ForeignKey("file_id")]
        public FileModel File { get; set; }

        /// <summary>
        /// ИД пользователя, который отправил работу.
        /// </summary>
        [Required]
        [Column("user_id")]
        public int UserId { get; set; }

        /// <summary>
        /// ИД задания, к которому прикреплена работа.
        /// </summary>
        [Required]
        [Column("task_id")]
        public int TaskId { get; set; }

        /// <summary>
        /// Задание, к которому прикреплена работа.
        /// </summary>
        [Required]
        [ForeignKey("task_id")]
        public TaskModel Task { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        [Column("created_at")]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Required]
        [Column("updated_at")]
        public DateTimeOffset Updated { get; set; }
    }
}

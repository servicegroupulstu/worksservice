﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using worksservice.Models.Main;

namespace worksservice.Models.Work
{
    /// <summary>
    /// Модель задания.
    /// </summary>
    [Table("tasks", Schema = "work")]
    public class TaskModel
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        [Column("id")]
        public int Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        [Required]
        [Column("name")]
        public string Name { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        [Column("description")]
        public string Description { get; set; }

        /// <summary>
        /// Сложность.
        /// </summary>
        [Required]
        [Column("complexity")]
        public int Complexity { get; set; }

        /// <summary>
        /// ИД предмета, к которому прикреплено задание.
        /// </summary>
        [Required]
        [Column("subject_id")]
        public int SubjectId { get; set; }

        /// <summary>
        /// Предмет, к которому прикреплено задание.
        /// </summary>
        [Required]
        [ForeignKey("subject_id")]
        public SubjectModel Subject { get; set; }

        /// <summary>
        /// Список файлов.
        /// </summary>
        public List<TaskFileModel> AttachmentFiles { get; set; }

        /// <summary>
        /// Список работ, которые сдали студенты.
        /// </summary>
        public List<WorkModel> Works { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        [Column("created_at")]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Required]
        [Column("updated_at")]
        public DateTimeOffset Updated { get; set; }
    }
}

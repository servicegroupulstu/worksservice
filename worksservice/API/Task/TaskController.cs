﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using worksservice.Attributies;
using worksservice.Constants;
using worksservice.Models.Work;
using worksservice.Repositories;
using worksservice.Services;

namespace worksservice.API.Task
{
    /// <summary>
    /// Управление заданиями преподователя.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IFileRepository _fileRepository;
        private readonly ITaskFileRepository _taskFileRepository;
        private readonly IFileService _fileService;

        public TaskController(ITaskRepository taskRepository, IFileRepository fileRepository, IFileService fileService, ITaskFileRepository taskFileRepository)
        {
            _taskRepository = taskRepository;
            _fileRepository = fileRepository;
            _fileService = fileService;
            _taskFileRepository = taskFileRepository;
        }

        /// <summary>
        /// Получение списка заданий.
        /// </summary>
        /// <param name="request">Данные, необходимые для поулчения списка заданий.</param>
        /// <returns>Список заданий.</returns>
        [HttpGet]
        [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher, UserRoles.Student)]
        public ActionResult<TaskListResponse> List([FromQuery] TaskListRequest request)
        {
            var list = _taskRepository.GetAll();

            // Filters
            if (request.SubjectId.HasValue)
                list = list.Where(i => i.SubjectId == request.SubjectId.Value);

            var totalCount = list.Count();

            // Pagination
            if (request.Offset.HasValue)
                list = list.Skip(request.Offset.Value);
            if (request.Limit.HasValue)
                list = list.Take(request.Limit.Value);

            var tasks = list.Select(i => new TaskInfo
            {
                Id = i.Id,
                Name = i.Name,
                Description = i.Description,
                Complexity = i.Complexity,
                SubjectId = i.SubjectId,
                WorksId = i.Works.Select(k => k.Id).ToArray(),
                Created = i.Created,
                Updated = i.Updated,
                Files = i.AttachmentFiles.Select(k => new File.FileInfo
                {
                    Id = k.File.Id,
                    DisplayName = k.File.DisplayName,
                    Extension = Path.GetExtension(k.File.Name),
                    ContentType = k.File.ContentType
                }).ToList()
                
            });

            return new TaskListResponse
            {
                Tasks = tasks.ToList(),
                TotalCount = totalCount
            };
        }

        /// <summary>
        /// Получение подробностей о задании.
        /// </summary>
        /// <param name="id">ИД задания.</param>
        /// <returns>Подробности о задании.</returns>
        [HttpGet("{id}")]
        [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher, UserRoles.Student)]
        public ActionResult<TaskDetailsResponse> Details([FromRoute] int id)
        {
            var item = _taskRepository.Get(id);
            if (item == default)
                return NotFound();

            return new TaskDetailsResponse
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Complexity = item.Complexity,
                SubjectId = item.SubjectId,
                WorksId = item.Works.Select(k => k.Id).ToArray(),
                Created = item.Created,
                Updated = item.Updated,
                Files = item.AttachmentFiles.Select(k => new File.FileInfo
                {
                    Id = k.File.Id,
                    DisplayName = k.File.DisplayName,
                    Extension = Path.GetExtension(k.File.Name),
                    ContentType = k.File.ContentType
                }).ToList()
            };
        }

        /// <summary>
        /// Добавление нового задания к предмету.
        /// </summary>
        /// <param name="request">Данные, необходимые для добавления нового задания.</param>
        /// <param name="uploadedFiles">Файлы, прикрепленные к заданию.</param>
        /// <returns>ИД созданного задания.</returns>
        [HttpPost]
        [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<ActionResult<TaskAddResponse>> AddAsync([FromBody] TaskAddRequest request, IFormFileCollection uploadedFiles)
        {
            var task = new TaskModel
            {
                Name = request.Name,
                Description = request.Description,
                Complexity = request.Complexity,
                SubjectId = request.SubjectId
            };
            await _taskRepository.AddAsync(task);

            await UploadAttachmentFiles(uploadedFiles, task.Id);

            return new TaskAddResponse
            {
                TaskId = task.Id
            };
        }

        /// <summary>
        /// Редактирование задания.
        /// </summary>
        /// <param name="id">ИД задания.</param>
        /// <param name="request">Данные, необходимые для редактирования задания.</param>
        /// <param name="uploadedFiles">Загруженные файлы.</param>
        [HttpPut("{id}")]
        [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<ActionResult> EditAsync([FromRoute] int id, [FromBody] TaskEditRequest request, IFormFileCollection uploadedFiles)
        {
            var item = _taskRepository.Get(id);
            if (item == default)
                return NotFound();

            if (!string.IsNullOrEmpty(request.Name))
                item.Name = request.Name;

            if (!string.IsNullOrEmpty(request.Description))
                item.Description = request.Description;

            if (request.Complexity.HasValue)
                item.Complexity = request.Complexity.Value;

            // delete files
            await DeleteAttachmentFiles(item.AttachmentFiles);

            // add new files
            await UploadAttachmentFiles(uploadedFiles, item.Id);

            await _taskRepository.UpdateAsync(item);

            return Ok();
        }

        /// <summary>
        /// Удаление задания.
        /// </summary>
        /// <param name="id">ИД задания.</param>
        [HttpDelete("{id}")]
        [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher)]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            var item = _taskRepository.Get(id);
            if (item == default)
                return NotFound();

            // delete files
            await DeleteAttachmentFiles(item.AttachmentFiles);

            await _taskRepository.DeleteAsync(item);

            return Ok();
        }

        private async System.Threading.Tasks.Task DeleteAttachmentFiles(List<TaskFileModel> attachmentFiles)
        {
            var filesToDelete = new List<FileModel>();
            foreach (var attachmentFile in attachmentFiles)
            {
                var fileToDelete = _fileRepository.Get(attachmentFile.File.Id);
                filesToDelete.Add(fileToDelete);
                await _fileService.DeleteAsync(fileToDelete.Name);
            }
            await _taskFileRepository.DeleteRangeAsync(attachmentFiles);
            await _fileRepository.DeleteRangeAsync(filesToDelete);
        }

        private async System.Threading.Tasks.Task UploadAttachmentFiles(IFormFileCollection uploadedFiles, int taskId)
        {
            foreach (var uploadedFile in uploadedFiles)
            {
                // upload file
                var fileName = await _fileService.UploadAsync(uploadedFile);
                var file = new FileModel
                {
                    Name = fileName,
                    DisplayName = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                    ContentType = uploadedFile.ContentType
                };

                await _fileRepository.AddAsync(file);

                var taskFile = new TaskFileModel
                {
                    TaskId = taskId,
                    FileId = file.Id
                };
                await _taskFileRepository.AddAsync(taskFile);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Task
{
    /// <summary>
    /// Ответ на запрос получения списка заданий.
    /// </summary>
    public class TaskListResponse
    {
        /// <summary>
        /// Список заданий.
        /// </summary>
        [Required]
        public List<TaskInfo> Tasks { get; set; }

        /// <summary>
        /// Общее количество записей.
        /// </summary>
        [Required]
        public int TotalCount { get; set; }
    }
}

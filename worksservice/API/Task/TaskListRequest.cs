﻿namespace worksservice.API.Task
{
    /// <summary>
    /// Запрос на получение списка заданий.
    /// </summary>
    public class TaskListRequest
    {
        /// <summary>
        /// Максимальное количество элементов в списке.
        /// </summary>
        public int? Limit { get; set; }

        /// <summary>
        /// Смещение относительно начала списка.
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// ИД предмета, к которому прикреплено задание.
        /// </summary>
        public int? SubjectId { get; set; }
    }
}

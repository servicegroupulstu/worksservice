﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using worksservice.API.File;

namespace worksservice.API.Task
{
    /// <summary>
    /// Информация о задании.
    /// </summary>
    public class TaskInfo
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сложность.
        /// </summary>
        [Required]
        public int Complexity { get; set; }

        /// <summary>
        /// ИД предмета, к которому прикреплено задание.
        /// </summary>
        [Required]
        public int SubjectId { get; set; }

        /// <summary>
        /// Список файлов.
        /// </summary>
        public List<FileInfo> Files { get; set; }

        /// <summary>
        /// Список работ, которые сдали студенты.
        /// </summary>
        public int[] WorksId { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Required]
        public DateTimeOffset Updated { get; set; }
    }
}

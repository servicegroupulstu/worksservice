﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Task
{
    /// <summary>
    /// Ответ на запрос добавления нового задания.
    /// </summary>
    public class TaskAddResponse
    {
        /// <summary>
        /// ИД созданного задания.
        /// </summary>
        [Required]
        public int TaskId { get; set; }
    }
}

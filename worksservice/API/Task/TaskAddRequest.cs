﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Task
{
    /// <summary>
    /// Данные для запроса добавления нового задания.
    /// </summary>
    public class TaskAddRequest
    {
        /// <summary>
        /// Название.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сложность.
        /// </summary>
        [Required]
        public int Complexity { get; set; }

        /// <summary>
        /// ИД предмета, к которому прикреплено задание.
        /// </summary>
        [Required]
        public int SubjectId { get; set; }
    }
}

﻿namespace worksservice.API.Task
{
    /// <summary>
    /// Данные для запроса редактирования задания.
    /// </summary>
    public class TaskEditRequest
    {
        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сложность.
        /// </summary>
        public int? Complexity { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Запрос на оценку работы студента преподавателем.
    /// </summary>
    public class WorkEvaluateRequest
    {
        /// <summary>
        /// Работа выполнена.
        /// </summary>
        [Required]
        public bool IsDone { get; set; }

        /// <summary>
        /// Оценка.
        /// </summary>
        public int? Mark { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Comment { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Данные для запроса редактирования работы студента.
    /// </summary>
    public class WorkEditRequest
    {
        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сложность работы.
        /// </summary>
        public int? Complexity { get; set; }
    }
}

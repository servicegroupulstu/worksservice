﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Ответ на запрос отчета по предмету.
    /// </summary>
    public class WorkReportBySubjectResponse
    {
        /// <summary>
        /// Список отчетов по предмету.
        /// </summary>
        [Required]
        public List<ReportBySubjectInfo> Reports { get; set; }

        /// <summary>
        /// Общее количество отчетов.
        /// </summary>
        [Required]
        public int TotalCount { get; set; }
    }

    /// <summary>
    /// Информация об отчете по предмету.
    /// </summary>
    public class ReportBySubjectInfo
    {
        /// <summary>
        /// ИД пользователя.
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Количество отправленных работ.
        /// </summary>
        [Required]
        public int SentedWorksCount { get; set; }

        /// <summary>
        /// Количество сданных работ.
        /// </summary>
        [Required]
        public int CompletedWorksCount { get; set; }

        /// <summary>
        /// Общее количество работ.
        /// </summary>
        [Required]
        public int TotalTasksCount { get; set; }

        /// <summary>
        /// Средняя оценка по предмету.
        /// </summary>
        [Required]
        public double AverageMark { get; set; }
    }
}

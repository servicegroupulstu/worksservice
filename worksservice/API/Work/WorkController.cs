﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using worksservice.Attributies;
using worksservice.Constants;
using worksservice.Models.Work;
using worksservice.Repositories;
using worksservice.Services;

namespace worksservice.API.Work
{
    /// <summary>
    /// Управление работами студентов.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher, UserRoles.Student)]
    public class WorkController : ControllerBase
    {
        private readonly IWorkRepository _workRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IFileService _fileService;

        public WorkController(IWorkRepository workRepository, IFileRepository fileRepository, IFileService fileService)
        {
            _workRepository = workRepository;
            _fileRepository = fileRepository;
            _fileService = fileService;
        }

        /// <summary>
        /// Получение списка работ студентов.
        /// </summary>
        /// <param name="request">Данные необходимые для выборки списка работ студентов.</param>
        /// <returns>Список работ студентов.</returns>
        [HttpGet]
        public ActionResult<WorkListResponse> List([FromQuery] WorkListRequest request)
        {
            var list = _workRepository.GetAll().AsQueryable();

            // Filtering
            if (request.SubjectId.HasValue)
                list = list.Where(i => i.Task.SubjectId == request.SubjectId.Value);

            if (request.UserId.HasValue)
                list = list.Where(i => i.UserId == request.UserId.Value);

            // TODO: add filter by group

            var works = list.Select(i => new WorkInfo
            {
                Id = i.Id,
                Description = i.Description,
                IsDone = i.IsDone,
                Mark = i.Mark,
                Comment = i.Comment,
                File = new File.FileInfo
                {
                    Id = i.File.Id,
                    DisplayName = i.File.DisplayName,
                    Extension = Path.GetExtension(i.File.Name)
                },
                UserId = i.UserId,
                Task = new Task.TaskInfo
                {
                    Id = i.Task.Id,
                    Name = i.Task.Name
                },
                Created = i.Created,
                Updated = i.Updated
            });

            var totalCount = works.Count();

            // Pagination
            if(request.Offset.HasValue)
                works = works.Skip(request.Offset.Value);
            if (request.Limit.HasValue)
                works = works.Take(request.Limit.Value);

            return new WorkListResponse
            {
                Works = works.ToList(),
                TotalCount = totalCount
            };
        }

        /// <summary>
        /// Получение подробностей о работе студента.
        /// </summary>
        /// <param name="id">ИД работы студента.</param>
        /// <returns>Подробности о работе студента.</returns>
        [HttpGet("{id}")]
        public ActionResult<WorkDetailsResponse> Details([FromRoute] int id)
        {
            var item = _workRepository.Get(id);
            if (item == default)
                return NotFound();

            return new WorkDetailsResponse
            {
                Id = item.Id,
                Description = item.Description,
                IsDone = item.IsDone,
                Mark = item.Mark,
                File = new API.File.FileInfo
                {
                    Id = item.File.Id,
                    DisplayName = item.File.DisplayName,
                    Extension = Path.GetExtension(item.File.Name)
                },
                UserId = item.UserId,
                Task = new Task.TaskInfo
                {
                    Id = item.Task.Id,
                    Name = item.Task.Name
                },
                Created = item.Created,
                Updated = item.Updated
            };
        }

        /// <summary>
        /// Добавление новой работы студента.
        /// </summary>
        /// <param name="request">Данные, необходимые для добавления работы студента.</param>
        /// <param name="uploadedFile">Файл.</param>
        /// <returns>ИД добавленной работы студента.</returns>
        [HttpPost]
        public async Task<ActionResult<WorkAddResponse>> AddAsync([FromBody] WorkAddRequest request, IFormFile uploadedFile)
        {
            if (uploadedFile == default)
                return BadRequest("File is empty.");

            // upload file
            var fileName = await _fileService.UploadAsync(uploadedFile);
            var file = new FileModel
            {
                Name = fileName,
                DisplayName = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                ContentType = uploadedFile.ContentType
            };

            await _fileRepository.AddAsync(file);

            // add work
            var item = new WorkModel
            {
                Description = request.Description,
                UserId = request.UserId,
                TaskId = request.TaskId,
                FileId = file.Id
            };
            await _workRepository.AddAsync(item);

            return new WorkAddResponse 
            { 
                WorkId = item.Id, 
                FileId = file.Id
            };
        }

        /// <summary>
        /// Редактирование работы студента.
        /// </summary>
        /// <param name="id">ИД работы студента.</param>
        /// <param name="request">Данные, необходимые для редактирования работы студента.</param>
        [HttpPut("{id}")]
        public async Task<ActionResult> EditAsync([FromRoute] int id, [FromBody] WorkEditRequest request)
        {
            var item = _workRepository.Get(id);
            if (item == default)
                return NotFound();

            if (!string.IsNullOrEmpty(request.Description))
                item.Description = request.Description;

            await _workRepository.UpdateAsync(item);
            
            return Ok();
        }

        /// <summary>
        /// Удаление работы студента.
        /// </summary>
        /// <param name="id">ИД работы студента.</param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            var work = _workRepository.Get(id);
            if (work == default)
                return NotFound();

            // TODO: Проверка принадлежности работы данному студенту.

            var file = _fileRepository.Get(work.FileId);
            if (file != default)
            {
                await _fileService.DeleteAsync(file.Name);
                await _fileRepository.DeleteAsync(file);
            }

            await _workRepository.DeleteAsync(work);

            return Ok();
        }

        /// <summary>
        /// Оценить работу студента.
        /// </summary>
        /// <param name="id">ИД работы студента.</param>
        /// <param name="request">Данные, необходимые для оценки работы студента.</param>
        [HttpPost("evaluate/{id}")]
        public async Task<ActionResult> EvaluateAsync([FromRoute] int id, [FromBody] WorkEvaluateRequest request)
        {
            var item = _workRepository.Get(id);
            if (item == default)
                return NotFound();

            if (item.IsDone.HasValue)
                return BadRequest("Работа уже оценена преподователем.");

            // TODO: Провека имеет ли право данный преподаватель оценивать эту работу.

            item.IsDone = request.IsDone;
            item.Mark = request.Mark;
            item.Comment = request.Comment;

            await _workRepository.UpdateAsync(item);

            return Ok();
        }

        /// <summary>
        /// Получение отчета всех студентов по предмету.
        /// </summary>
        /// <param name="subjectId">ИД предмета.</param>
        /// <returns>Отчет по предмету.</returns>
        [HttpGet("report-by-subject/{subjectId}")]
        public ActionResult<WorkReportBySubjectResponse> ReportBySubject([FromRoute] int subjectId)
        {
            var list = _workRepository.GetAll().AsQueryable();
            list = list.Where(i => i.Task.SubjectId == subjectId);

            var groupList = (from l in list
                          group l by l.UserId into r
                          select new
                          {
                              UserId = r.Key,
                              Count = r.Count(),
                              Works =  from w in r select w
                          }).ToList();

            var reports = new List<ReportBySubjectInfo>();
            foreach(var item in groupList)
            {
                if (item.Works.Count() == 0)
                    continue;

                var marks = item.Works.Where(i => i.Mark.HasValue).Select(i => i.Mark.Value);
                reports.Add(new ReportBySubjectInfo
                {
                    UserId = item.UserId,
                    SentedWorksCount = item.Count,
                    CompletedWorksCount = item.Works.Where(i => i.IsDone.HasValue && i.IsDone.Value).Count(),
                    AverageMark = marks.Count() > 0 ? marks.Average() : 0,
                    TotalTasksCount = item.Works.Select(i => i.Task).Count()
                });
            }

            var totalCount = reports.Count();

            return new WorkReportBySubjectResponse
            {
                Reports = reports,
                TotalCount = totalCount
            };
        }

        /// <summary>
        /// Получение отчета всех предметов по студенту.
        /// </summary>
        /// <param name="userId">ИД студента.</param>
        /// <returns>Отчет по студенту.</returns>
        [HttpGet("report-by-user/{userId}")]
        public ActionResult<WorkReportByUserResponse> ReportByUser([FromRoute] int userId)
        {
            var list = _workRepository.GetAll().AsQueryable();
            list = list.Where(i => i.UserId == userId);

            var groupList = (from l in list
                             group l by l.Task.SubjectId into r
                             select new
                             {
                                 SubjectId = r.Key,
                                 Count = r.Count(),
                                 Works = from w in r select w
                             }).ToList();

            var reports = new List<WorkReportByUserResponse.ReportByUserInfo>();
            foreach (var item in groupList)
            {
                if (item.Works.Count() == 0)
                    continue;

                var marks = item.Works.Where(i => i.Mark.HasValue).Select(i => i.Mark.Value);
                reports.Add(new WorkReportByUserResponse.ReportByUserInfo
                {
                    SubjectId = item.SubjectId,
                    SentedWorksCount = item.Count,
                    CompletedWorksCount = item.Works.Where(i => i.IsDone.HasValue && i.IsDone.Value).Count(),
                    AverageMark = marks.Count() > 0 ? marks.Average() : 0,
                    TotalTasksCount = item.Works.Select(i => i.Task).Count()
                });
            }

            var totalCount = reports.Count();

            return new WorkReportByUserResponse
            {
                Reports = reports,
                TotalCount = totalCount
            };
        }
    }
}
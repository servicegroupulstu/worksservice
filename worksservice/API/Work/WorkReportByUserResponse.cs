﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Ответ на запрос отчета по студенту.
    /// </summary>
    public class WorkReportByUserResponse
    {
        /// <summary>
        /// Список отчетов по студенту.
        /// </summary>
        [Required]
        public List<ReportByUserInfo> Reports { get; set; }

        /// <summary>
        /// Общее количество отчетов.
        /// </summary>
        [Required]
        public int TotalCount { get; set; }

        /// <summary>
        /// Информация об отчете по студенту.
        /// </summary>
        public class ReportByUserInfo
        {
            /// <summary>
            /// ИД предмета.
            /// </summary>
            [Required]
            public int SubjectId { get; set; }

            /// <summary>
            /// Количество отправленных работ.
            /// </summary>
            [Required]
            public int SentedWorksCount { get; set; }

            /// <summary>
            /// Количество сданных работ.
            /// </summary>
            [Required]
            public int CompletedWorksCount { get; set; }

            /// <summary>
            /// Общее количество работ.
            /// </summary>
            [Required]
            public int TotalTasksCount { get; set; }

            /// <summary>
            /// Средняя оценка по предмету.
            /// </summary>
            [Required]
            public double AverageMark { get; set; }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Запрос на добавление новой работы студента.
    /// </summary>
    public class WorkAddRequest
    {
        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ИД пользователя, который отправил работу.
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// ИД задания, к которому прикреплена работа.
        /// </summary>
        [Required]
        public int TaskId { get; set; }
    }
}

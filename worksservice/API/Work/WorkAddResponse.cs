﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Ответ на запрос добавления новой работы студента.
    /// </summary>
    public class WorkAddResponse
    {
        /// <summary>
        /// ИД работы студента.
        /// </summary>
        [Required]
        public int WorkId { get; set; }

        /// <summary>
        /// ИД файла.
        /// </summary>
        [Required]
        public int FileId { get; set; }
    }
}

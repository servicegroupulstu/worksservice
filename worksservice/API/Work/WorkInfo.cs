﻿using System;
using System.ComponentModel.DataAnnotations;
using worksservice.API.File;
using worksservice.API.Task;

namespace worksservice.API.Work
{
    /// <summary>
    /// Информация о работе студента.
    /// </summary>
    public class WorkInfo
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Выполнена.
        /// </summary>
        public bool? IsDone { get; set; }

        /// <summary>
        /// Оценка.
        /// </summary>
        public int? Mark { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Файл.
        /// </summary>
        [Required]
        public FileInfo File { get; set; }

        /// <summary>
        /// ИД пользователя, который отправил работу.
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// ИД задания, к которому прикреплена работа.
        /// </summary>
        [Required]
        public int TaskId { get; set; }

        /// <summary>
        /// Задание, к которому прикреплена работа.
        /// </summary>
        public TaskInfo Task { get; set; }

        /// <summary>
        /// Дата и время создания записи.
        /// </summary>
        [Required]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Дата и время обновления записи.
        /// </summary>
        [Required]
        public DateTimeOffset Updated { get; set; }
    }
}

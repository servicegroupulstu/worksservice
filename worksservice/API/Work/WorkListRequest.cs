﻿namespace worksservice.API.Work
{
    /// <summary>
    /// Данные, необходимые для получения запроса списка работ студентов.
    /// </summary>
    public class WorkListRequest
    {
        /// <summary>
        /// Максимальное количество элементов в списке.
        /// </summary>
        public int? Limit { get; set; }
        
        /// <summary>
        /// Смещение относительно начала списка.
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// ИД предмета.
        /// </summary>
        public int? SubjectId { get; set; }

        /// <summary>
        /// ИД пользователя.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// ИД группы.
        /// </summary>
        public int? GroupId { get; set; }
    }
}

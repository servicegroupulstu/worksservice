﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace worksservice.API.Work
{
    /// <summary>
    /// Ответ на запрос получения списка работ студентов.
    /// </summary>
    public class WorkListResponse
    {
        /// <summary>
        /// Список работ студентов.
        /// </summary>
        [Required]
        public List<WorkInfo> Works { get; set; }

        /// <summary>
        /// ОБщее количество работ студентов.
        /// </summary>
        [Required]
        public int TotalCount { get; set; }
    }
}

﻿namespace worksservice.API.File
{
    /// <summary>
    /// Данные для запроса получения списка файлов.
    /// </summary>
    public class FileListRequest
    {
        /// <summary>
        /// Максимальное количество элементов в списке.
        /// </summary>
        public int? Limit { get; set; }

        /// <summary>
        /// Смещение относительно начала списка.
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Название файла.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Расширение файла.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Список ИД файлов, которые необходимо отобрать.
        /// </summary>
        public int[] FilesIds { get; set; }
    }
}

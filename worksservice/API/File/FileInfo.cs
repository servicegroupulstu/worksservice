﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.File
{
    /// <summary>
    /// Информация о файле.
    /// </summary>
    public class FileInfo
    {
        /// <summary>
        /// ИД записи.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Отображаемое название файла.
        /// </summary>
        [Required]
        public string DisplayName { get; set; }

        /// <summary>
        /// Расширение файла.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Тип контента.
        /// </summary>
        public string ContentType { get; set; }
    }
}

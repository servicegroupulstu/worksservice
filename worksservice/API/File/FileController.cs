﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using worksservice.Attributies;
using worksservice.Constants;
using worksservice.Models.Work;
using worksservice.Repositories;
using worksservice.Services;

namespace worksservice.API.File
{
    /// <summary>
    /// Управление файлами.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    [ApiAuthorize(UserRoles.Admin, UserRoles.Teacher, UserRoles.Student)]
    public class FileController : ControllerBase
    {
        private readonly IFileRepository _fileRepository;
        private readonly IFileService _fileService;

        public FileController(IFileRepository fileRepository, IFileService fileService)
        {
            _fileRepository = fileRepository;
            _fileService = fileService;
        }

        /// <summary>
        /// Получение списка файлов.
        /// </summary>
        /// <param name="request">Данные, необходимые для получения списка файлов.</param>
        /// <returns>Список файлов.</returns>
        [HttpGet]
        public ActionResult<FileListResponse> List([FromQuery] FileListRequest request)
        {
            var list = _fileRepository.GetAll().AsQueryable();

            if (!string.IsNullOrEmpty(request.Name))
                list = list.Where(i => i.Name == request.Name);

            if (!string.IsNullOrEmpty(request.Extension))
                list = list.Where(i => Path.GetExtension(i.Name) == request.Extension);

            var files = list.Select(i => new FileInfo
            {
                Id = i.Id,
                DisplayName = i.DisplayName,
                ContentType = i.ContentType,
                Extension = Path.GetExtension(i.Name)
            }).AsEnumerable();

            if (request.FilesIds != default && request.FilesIds.Length > 0)
                files = files.Where(i => request.FilesIds.Contains(i.Id));

            var totalCount = files.Count();

            if (request.Offset.HasValue)
                files = files.Skip(request.Offset.Value);
            if (request.Limit.HasValue)
                files = files.Take(request.Limit.Value);

            return new FileListResponse
            {
                Files = files.ToList(),
                TotalCount = totalCount
            };
        }

        /// <summary>
        /// Получение подробнстей о файле.
        /// </summary>
        /// <param name="id">ИД файла.</param>
        /// <returns>Подробности о файле.</returns>
        [HttpGet("details/{id}")]
        public ActionResult<FileDetailsResponse> Details([FromRoute] int id)
        {
            var item = _fileRepository.Get(id);
            if (item == default)
                return NotFound();

            return new FileDetailsResponse
            {
                Id = item.Id,
                DisplayName = item.DisplayName,
                Extension = Path.GetExtension(item.Name),
                ContentType = item.ContentType
            };
        }

        /// <summary>
        /// Загрузка файла.
        /// </summary>
        /// <param name="uploadedFile">Файл.</param>
        /// <returns>ИД загруженного файла.</returns>
        [HttpPost]
        public async Task<ActionResult<FileUploadResponse>> UploadAsync(IFormFile uploadedFile)
        {
            if (uploadedFile == default)
                return BadRequest("File is null");

            var fileName = await _fileService.UploadAsync(uploadedFile);
            var file = new FileModel
            {
                Name = fileName,
                DisplayName = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                ContentType = uploadedFile.ContentType,
                Created = DateTimeOffset.Now,
                Updated = DateTimeOffset.Now
            };

            await _fileRepository.AddAsync(file);

            return new FileUploadResponse
            {
                FileId = file.Id
            };
        }

        /// <summary>
        /// Выгрузка файла.
        /// </summary>
        /// <param name="id">ИД файла.</param>
        /// <returns>Контент файла.</returns>
        [HttpGet("download/{id}")]
        public async Task<ActionResult> DownloadAsync([FromRoute] int id)
        {
            var file = _fileRepository.Get(id);
            if (file == default)
                return NotFound();

            var content = await _fileService.GetAsync(file.Name);
            return File(content, file.ContentType, file.Name);
        }

        /// <summary>
        /// Удаление файла.
        /// </summary>
        /// <param name="id">ИД файла.</param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            var item = _fileRepository.Get(id);
            if (item == default)
                return NotFound();

            await _fileService.DeleteAsync(item.Name);

            await _fileRepository.DeleteAsync(item);

            return Ok();
        }
    }
}
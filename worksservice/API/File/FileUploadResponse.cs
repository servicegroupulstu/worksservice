﻿using System.ComponentModel.DataAnnotations;

namespace worksservice.API.File
{
    /// <summary>
    /// Ответ за запрос загрузки файла.
    /// </summary>
    public class FileUploadResponse
    {
        /// <summary>
        /// ИД Файла.
        /// </summary>
        [Required]
        public int FileId { get; set; }
    }
}

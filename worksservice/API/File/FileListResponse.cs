﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace worksservice.API.File
{
    /// <summary>
    /// Ответ на запрос получения списка файлов.
    /// </summary>
    public class FileListResponse
    {
        /// <summary>
        /// Список файлов.
        /// </summary>
        [Required]
        public List<FileInfo> Files { get; set; }

        /// <summary>
        /// Общее количество файлов.
        /// </summary>
        [Required]
        public int TotalCount { get; set; }
    }
}

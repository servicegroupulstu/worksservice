﻿namespace worksservice.Constants
{
    public static class UserRoles
    {
        public const string Admin = "admin";
        public const string Student = "student";
        public const string Teacher = "teacher";
    }
}

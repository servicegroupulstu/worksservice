﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace worksservice.Attributies
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string[] _roles;

        public ApiAuthorizeAttribute(params string[] roles)
        {
            _roles = roles;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var isAuthorize = context.HttpContext.User.Identity.IsAuthenticated;
            if (!isAuthorize)
            {
                context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }

            foreach(var role in _roles)
            {
                if (!string.IsNullOrEmpty(role) && !context.HttpContext.User.IsInRole(role))
                {
                    context.Result = new JsonResult(new { message = "Invalid user role" }) { StatusCode = StatusCodes.Status400BadRequest };
                }
            }
            
        }
    }
}

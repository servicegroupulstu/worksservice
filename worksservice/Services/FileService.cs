﻿using Microsoft.AspNetCore.Http;
using Minio;
using System;
using System.IO;
using System.Threading.Tasks;
using worksservice.Constants;

namespace worksservice.Services
{
    /// <summary>
    /// Сервис для работы с файлами.
    /// </summary>
    public class FileService : IFileService
    {
        private readonly MinioClient _minioClient;
        private readonly FileUploadSettings _fileUploadSettings;

        public FileService(MinioClient minioClient, FileUploadSettings fileUploadSettings)
        {
            _minioClient = minioClient;
            _fileUploadSettings = fileUploadSettings;
        }

        public async Task<string> UploadAsync(IFormFile uploadedFile)
        {
            var extension = Path.GetExtension(uploadedFile.FileName);
            var fileName = Guid.NewGuid().ToString() + extension;

            // Make a bucket on the server, if not already present.
            bool found = await _minioClient.BucketExistsAsync(_fileUploadSettings.BucketName);
            if (!found)
                await _minioClient.MakeBucketAsync(_fileUploadSettings.BucketName, _fileUploadSettings.Location);

            // Upload a file to bucket.
            await _minioClient.PutObjectAsync(_fileUploadSettings.BucketName, fileName, uploadedFile.OpenReadStream(), uploadedFile.Length, uploadedFile.ContentType);

            return fileName;
        }

        public async Task<Stream> GetAsync(string fileName)
        {
            bool found = await _minioClient.BucketExistsAsync(_fileUploadSettings.BucketName);
            if (!found)
                throw new Exception("Контейнер не существует.");

            Stream fileStream = null;
            await _minioClient.GetObjectAsync(_fileUploadSettings.BucketName, fileName, (stream) => { fileStream = stream; });

            return fileStream;
        }

        public async Task DeleteAsync(string fileName)
        {
            bool found = await _minioClient.BucketExistsAsync(_fileUploadSettings.BucketName);
            if (!found)
                throw new Exception("Контейнер не существует.");

            await _minioClient.RemoveObjectAsync(_fileUploadSettings.BucketName, fileName);
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace worksservice.Services
{
    /// <summary>
    /// Интерфейс сервиса для работы с файлами.
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// Загрузка файла.
        /// </summary>
        /// <param name="uploadFile">Файл.</param>
        /// <returns>Имя загруженного файла.</returns>
        Task<string> UploadAsync(IFormFile uploadFile);

        /// <summary>
        /// Получение файла.
        /// </summary>
        /// <param name="fileName">Имя файла.</param>
        /// <returns>Контент файла.</returns>
        Task<Stream> GetAsync(string fileName);

        /// <summary>
        /// Удаление файла.
        /// </summary>
        /// <param name="pathToFile">Путь до файла.</param>
        Task DeleteAsync(string pathToFile);
    }
}

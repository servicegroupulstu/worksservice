﻿using Microsoft.EntityFrameworkCore;
using worksservice.Models.Work;

namespace worksservice.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<FileModel> Files { get; set; }
        public DbSet<WorkModel> Works { get; set; }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<TaskFileModel> TaskFiles { get; set; }
    }
}

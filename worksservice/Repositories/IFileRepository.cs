﻿using System.Collections.Generic;
using System.Threading.Tasks;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    /// <summary>
    /// Интерфейс для управления хранения информации о файле в БД.
    /// </summary>
    public interface IFileRepository : IRepository<FileModel>
    {
        Task DeleteRangeAsync(List<FileModel> models);
    }
}

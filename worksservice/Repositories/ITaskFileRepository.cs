﻿using System.Collections.Generic;
using System.Threading.Tasks;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    public interface ITaskFileRepository : IRepository<TaskFileModel>
    {
        Task DeleteRangeAsync(List<TaskFileModel> models);
    }
}

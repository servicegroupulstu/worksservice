﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using worksservice.Data;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly DataContext _context;

        public TaskRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<TaskModel> GetAll()
        {
            return _context.Tasks
                .Include(i => i.Works)
                .Include(i => i.Subject)
                .Include(i => i.AttachmentFiles)
                    .ThenInclude(i => i.File);
        }

        public TaskModel Get(int id)
        {
            return _context.Tasks
                .Include(i => i.Works)
                .Include(i => i.Subject)
                .Include(i => i.AttachmentFiles)
                    .ThenInclude(i => i.File)
                .FirstOrDefault(i => i.Id == id);
        }

        public async Task AddAsync(TaskModel model)
        {
            model.Created = DateTimeOffset.Now;
            model.Updated = DateTimeOffset.Now;

            _context.Tasks.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TaskModel model)
        {
            model.Updated = DateTimeOffset.Now;

            _context.Tasks.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TaskModel model)
        {
            _context.Tasks.Remove(model);
            await _context.SaveChangesAsync();
        }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;

namespace worksservice.Repositories
{
    /// <summary>
    /// Интерфейс репозитория.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class 
    {
        /// <summary>
        /// Получение списка.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Получение.
        /// </summary>
        /// <param name="id">ИД задания</param>
        /// <returns>Задание.</returns>
        T Get(int id);

        /// <summary>
        /// Добавление.
        /// </summary>
        /// <param name="model">Задание</param>
        Task AddAsync(T model);

        /// <summary>
        /// Редактирование.
        /// </summary>
        /// <param name="model">Задание.</param>
        Task UpdateAsync(T model);

        /// <summary>
        /// Удаление.
        /// </summary>
        /// <param name="model">Задание.</param>
        Task DeleteAsync(T model);
    }
}

﻿using worksservice.Models.Work;

namespace worksservice.Repositories
{
    /// <summary>
    /// Интерфейс для управления информацией о работах студента в БД.
    /// </summary>
    public interface IWorkRepository : IRepository<WorkModel>
    {
    }
}

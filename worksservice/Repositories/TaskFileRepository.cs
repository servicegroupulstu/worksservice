﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using worksservice.Data;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    public class TaskFileRepository : ITaskFileRepository
    {
        private readonly DataContext _context;

        public TaskFileRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<TaskFileModel> GetAll()
        {
            return _context.TaskFiles
                .Include(i => i.File)
                .Include(i => i.Task);
        }

        public TaskFileModel Get(int id)
        {
            return _context.TaskFiles
                .Include(i => i.File)
                .Include(i => i.Task)
                .FirstOrDefault(i => i.TaskId == id);
        }

        public async Task AddAsync(TaskFileModel model)
        {
            _context.TaskFiles.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TaskFileModel model)
        {
            _context.TaskFiles.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TaskFileModel model)
        {
            _context.TaskFiles.Remove(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(List<TaskFileModel> models)
        {
            _context.RemoveRange(models);
            await _context.SaveChangesAsync();
        }
    }
}

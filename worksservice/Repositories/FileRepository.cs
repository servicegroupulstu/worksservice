﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using worksservice.Data;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    public class FileRepository : IFileRepository
    {
        private readonly DataContext _context;

        public FileRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<FileModel> GetAll()
        {
            return _context.Files.AsQueryable();
        }

        public FileModel Get(int id)
        {
            return _context.Files.FirstOrDefault(i => i.Id == id);
        }

        public async Task AddAsync(FileModel model)
        {
            model.Created = DateTimeOffset.Now;
            model.Updated = DateTimeOffset.Now;

            _context.Files.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(FileModel model)
        {
            model.Updated = DateTimeOffset.Now;

            _context.Files.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(FileModel model)
        {
            _context.Files.Remove(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(List<FileModel> models)
        {
            _context.RemoveRange(models);
            await _context.SaveChangesAsync();
        }
    }
}

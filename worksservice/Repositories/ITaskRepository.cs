﻿using worksservice.Models.Work;

namespace worksservice.Repositories
{
    /// <summary>
    /// Интерфейс для управления информацией о заданиях преподователя в БД.
    /// </summary>
    public interface ITaskRepository : IRepository<TaskModel>
    {
    }
}

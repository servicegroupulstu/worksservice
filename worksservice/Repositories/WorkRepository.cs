﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using worksservice.Data;
using worksservice.Models.Work;

namespace worksservice.Repositories
{
    public class WorkRepository : IWorkRepository
    {
        private readonly DataContext _context;

        public WorkRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<WorkModel> GetAll()
        {
            return _context.Works.Include(i => i.File).Include(i => i.Task).AsQueryable();
        }

        public WorkModel Get(int id)
        {
            return _context.Works.Include(i => i.File).Include(i => i.Task).FirstOrDefault(i => i.Id == id);
        }

        public async Task AddAsync(WorkModel model)
        {
            model.Created = DateTimeOffset.Now;
            model.Updated = DateTimeOffset.Now;

            _context.Works.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(WorkModel model)
        {
            model.Updated = DateTimeOffset.Now;

            _context.Works.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(WorkModel model)
        {
            _context.Works.Remove(model);
            await _context.SaveChangesAsync();
        }
    }
}
